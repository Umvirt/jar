<?php
ob_start();
include "inc/main.php";

function allnicscapacity(){
global $wgnics;
$capacity=0;
foreach ($wgnics as $k=>$nic){
$adrnet=explode('/',$nic['address']);

$addr=$adrnet[0];
$netmask=$adrnet[1];

$ni=new networkinfo($addr,$netmask);


$intfirstusableip=$ni->intzeroip+2; 
$intlastusableip=$ni->intlastip-1;

$hostscnt=$intlastusableip-$intfirstusableip+1;
//$ni->intlastip-$ni->intzeroip-1;

$capacity=$capacity+$hostscnt;


//var_dump($ni, $hostscnt, printbinasip(int2binary($intfirstusableip)),printbinasip(int2binary($intlastusableip)));
}
return $capacity;
}


function activeuserscnt(){
global $config;
global $db;
$res=0;
$sql="select count(id) cnt from (select c.id, c.timestamp,
now() currentdate, DATE_ADD(c.timestamp, INTERVAL ".$config['reservationtime']." second) releasedate,
unix_timestamp(DATE_ADD(c.timestamp, INTERVAL ".$config['reservationtime']." second))-unix_timestamp(now()) timerest,
p.ipaddr pipaddr, i.ipaddr, i.port, i.publickey, i.gwipaddr  from clients c
left join peers p on p.id=c.id
left join interfaces i on i.id=p.interface) as s where timerest>=0";
//echo $sql;
$db->execute($sql);
$x=$db->dataset;
if(is_array($x)){
$res=$x[0]['cnt'];
}
return $res;
}


function socktest($address, $port, $type){
$chk=true;
if($type=='tcp'){
$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
}
if($type=='udp'){
$socket = socket_create(AF_INET, SOCK_STREAM, SOL_UDP);
}
if($socket === false){
return false;
}

//socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, 1);
//socket_set_option($socket, SOL_SOCKET, SO_SNDTIMEO, 1);

$result = socket_connect($socket, $address, $port);
if($result === false){
$chk=false;
}
socket_close($socket);
return $chk;
}

function ping($address){
	$ping = exec('ping -c 1 '.gethostbyname($address));
	$chunks = explode(' ', $ping);
	//var_dump($ping,$chunks, $chunks[3]);
	$val= explode('/',$chunks[3]);
	$res = $val[1];
	//substr($chunks[14], 0, strlen($chunks[14])-2);
	return $res;
}

function getkeyid($key){
global $db;
$key=addslashes($key);
$sql="select id from clients where wgkey=\"$key\"";
//echo $sql;
$db->execute($sql);
$x=$db->dataset;
if(is_array($x)){
foreach($x as $row){
return $row['id'];
}
}
}

function getpeer($ip){
global $db;
global $config;
$ip=addslashes($ip);
$sql="select c.wgkey pubkey, c.ip, p.id, p.ipaddr, p.interface interface_id, i.name interface, i.gwipaddr gateway, i.name nicname,
now() currentdate, DATE_ADD(c.timestamp, INTERVAL ".$config['reservationtime']." second) releasedate,
unix_timestamp(DATE_ADD(c.timestamp, INTERVAL ".$config['reservationtime']." second))-unix_timestamp(now()) timerest
 from peers p
left join interfaces i on i.id=p.interface
left join clients c on c.id=p.id
where p.ipaddr=\"$ip\"";
//echo $sql;
$db->execute($sql);
$x=$db->dataset;
if(is_array($x)){
foreach($x as $row){
return $row;
}
}
}
/*
function getpeer($ip){
global $db;
$ip=addslashes($ip);
$sql="select p.id, c.ip, c.wgkey pubkey, i.name, i.gwipaddr gateway, i.name interface from peers p left join clients c on  c.id=p.id left join interfaces i on i.id=p.interface where p.ipaddr=\"$ip\"";
//echo $sql;
$db->execute($sql);
$x=$db->dataset;
if(is_array($x)){
foreach($x as $row){
return $row;
}
}
}
*/



$ip=$_SERVER['REMOTE_ADDR'];
$title="";

$act=@$_REQUEST['act'];


$errors=array();
switch ($act){

case "scanme":
$title="Simple TCP-port scanner";
$addr=$_SERVER['REMOTE_ADDR'];

if(!getpeer($ip)){
echo "Error: Access denied!";
break;
}

//echo $addr;

$t1=time();
//for($i=1;$i<=10;$i++){
$ports=array(22,2321,25,53,69,80,81,92,101,143,3128,443,3306,5432,5900,8080,9050);

$p=0;
foreach($ports as $i){
if(@socktest($addr, $i, 'tcp')){
//echo "<li>.$i";
$op[]=$i;
$p++;
//}
}
//echo "$i";
}

$t2=time();
echo "<br>Your IP-address: ".$addr;
echo "<br>Scanned ports: ".count($ports);
echo "<br>Opened ports: $p";
echo "<br>Consumed time: ".($t2-$t1);
if($p){
echo "<h2>Opened ports list</h2>";
echo "<ol>";
foreach($op as $v){
echo "<li>".$v;
}
echo "</ol>";

}
break;


case "about":
echo file_get_contents('html/about.html');
break;
case "services":
echo file_get_contents('html/services.html');
break;


case "capacity":
$title="VPN-server capacity";

$sql="select p.id, p.ipaddr, p.interface, i.name nicname,
now() currentdate, DATE_ADD(c.timestamp, INTERVAL ".$config['reservationtime']." second) releasedate,
unix_timestamp(DATE_ADD(c.timestamp, INTERVAL ".$config['reservationtime']." second))-unix_timestamp(now()) timerest
 from peers p
left join interfaces i on i.id=p.interface
left join clients c on c.id=p.id
";
//echo $sql;
$db->execute($sql);
$peers=$db->dataset;
//var_dump($peers);

echo "<b>Total slots</b>: ".allnicscapacity()."<br>";
echo "<b>Active keys</b>: ".activeuserscnt();
//var_dump($wgnics);
foreach ($wgnics as $k=>$nic){
$nicpeers=array();
$adrnet=explode('/',$nic['address']);

$addr=$adrnet[0];
$netmask=$adrnet[1];

$ni=new networkinfo($addr,$netmask);
/*
$binipval=binary($addr);
$binnetmask=binarynetmask($netmask);
$bin0ip=binarynetmaskaddr($binipval,$netmask,0);
$bin1ip=binarynetmaskaddr($binipval,$netmask,1);
$net=printbinasip($bin0ip);

$intzeroip=binary2int($bin0ip);
$intlastip=binary2int($bin1ip);
$hostscnt=$intlastip-$intzeroip-1;
*/
foreach($peers as $peer){
if($peer['nicname']=$k){
//$min=$peer['timerest']
//$mpeer=$peer;
$nicpeers[]=$peer;
}
}

if(count($nicpeers)){
$min=$nicpeers[0]['timerest'];
$mpeer=$nicpeers[0];

foreach($nicpeers as $peer){

if($peer['timerest']<=$min){
$min=$peer['timerest'];
$mpeer=$peer;
//$nicpeers[]=$peer['ipaddr'];
}

}
}



$net=$ni->net;

$intfirstusableip=$ni->intzeroip+2; 
$intlastusableip=$ni->intlastip-1;

$hostscnt=$intlastusableip-$intfirstusableip+1;


//$hostscnt=$ni->intlastip-$ni->intzeroip-1;

$usedhostscnt=count($nicpeers);
$freehostscnt=$hostscnt-$usedhostscnt;
$freehostspercent=floor($freehostscnt*100/$hostscnt);

//$ni->intlastip-$ni->intzeroip-1;

//$capacity=$capacity+$hostscnt;

//var_dump($nicpeers);
//var_dump($ni, $hostscnt, printbinasip(int2binary($intfirstusableip)),printbinasip(int2binary($intlastusableip)));


echo "<h2>$net/".$adrnet[1]."</h2>";




echo "Possible slots: ".$hostscnt."<br>";
echo "Reserved slots: $usedhostscnt<br>";
if($freehostscnt>=0){
echo "Free slots: $freehostscnt ($freehostspercent%)<br>";
}else{
echo "Error: overflow<br>";
}
if(count($nicpeers)){
$min=floor($min/60);
if($min>0){
echo "Nearest slot release will be occured in $min minutes<br>";
}
}

echo "<hr>Interface: $k<br>";
echo "First slot IP-address: ".printbinasip(int2binary($intfirstusableip))."<br>";
echo "Last slot IP-address: ".printbinasip(int2binary($intlastusableip))."<br>";


}

break;

case "connectionsettingsfrm":
$title="Connection settings";
echo "<form>
<label>Public key:<br><textarea name=wgkey></textarea></label>
<p class=\"help-text\" id=\"wgkeyHelpText\">Insert your Wirguard public key here. You can use clipboard.</p>
<input type=submit value=\"Get Settings\" class=\"button\"><input type=hidden name=act value=connectionsettings></form>";
break;

case "connectionsettings":
$title="Connection settings";
$key=addslashes(trim($_REQUEST['wgkey']));

$sql="select c.id, c.timestamp, 
now() currentdate, DATE_ADD(c.timestamp, INTERVAL ".$config['reservationtime']." second) releasedate, 
unix_timestamp(DATE_ADD(c.timestamp, INTERVAL ".$config['reservationtime']." second))-unix_timestamp(now()) timerest,
p.ipaddr pipaddr, i.ipaddr, i.port, i.publickey, i.gwipaddr,i.dns  from clients c 
left join peers p on p.id=c.id
left join interfaces i on i.id=p.interface
where wgkey=\"$key\"";
$db->execute($sql);
$x=$db->dataset;
//var_dump(count($x));
//echo $sql.$db->error;
if(count($x)){
foreach($x as $row){
if(intval($row['timerest'])>0){
if($row['publickey']){

$addr=$row['gwipaddr'];
echo "<h2>VPN-server</h2>";
echo "Public key: <br><pre><tt>".$row['publickey']."</tt></pre><br>";
echo "IP-adress: ".$row['ipaddr']."<br>";
echo "UDP-port: ".$row['port']."<br>";
echo "Current date: ". $row['currentdate']."<br>";
echo "<h2>VPN-client</h2>";
echo "IP-adress: ".$row['pipaddr']."<br>";
echo "Gateway & subnet: ".$addr."<br>";
if($row['dns']){
echo "DNS-server: ".$row['dns']."<br>";
}
echo "Reservation date: ".$row['timestamp']."<br>";
echo "Release date: ".$row['releasedate']."<br>";
echo "Rest: ".$row['timerest']."<br>";
/*
[Interface]
Address = 172.16.50.2/32
PrivateKey = ***secret***
DNS = 172.16.50.1

[Peer]
PublicKey = ***not secret***
Endpoint = 14.1.28.146:58827
AllowedIPs = 0.0.0.0/0
PersistentKeepalive = 21
*/

echo "<h2>Sample VPN-client config</h2>";
echo "<pre><tt>";
echo "# Virtual network interface settings.\n";
echo "[Interface]\n";
echo "# Assigned VPN-adress. Don't change this value.\n";
echo "Address = ".$row['pipaddr']."/32\n";
echo "# Your private key value. Replace with your actual wireguard private key.\n";
echo "PrivateKey = *** your private key ***\n";
if($row['dns']){
echo "# DNS-server (optional) can be raplaced by records in /etc/hosts file.\n";
echo "DNS = ".$row['dns']."\n";
}
echo "\n";
echo "# Connection settings.\n";
echo "[Peer]\n";
echo "# Server public key. Don't change this value.\n";
echo "PublicKey = ".$row['publickey']."\n";
echo "# Server IP-address. Don't change this value.\n";
echo "Endpoint = ".$row['ipaddr'].":".$row['port']."\n";
echo "# IP-addresses which accessible using VPN. Change this value only if you shure.\n";
echo "AllowedIPs = 0.0.0.0/0\n";
echo "# Connection refresh timeout. Change this value only if you shure. \n";
echo "PersistentKeepalive = 21\n";

echo "</tt></pre>";

}else{
echo "<p>Your request in proccess please wait a few minutes...<p>";
echo "<p>Reload page manualy.</p>";
}


}else{
echo "Error: Your reservation is over.";
echo "<br>";
echo "Do you wish to <a href=\"/?act=updatereservation&wgkey=".urlencode($key)."\">reserve access for this key again</a>?";
}


} 

}else{
echo "Error: Public key not found!";
}
break;


case "signupfrm":
$title="Sign up";
$rest=allnicscapacity()-activeuserscnt();
if($rest<=0){
echo "Server is full. If you wish to reserve a slot, try again later!";
break;
}

echo "<form>
<label>Public key:<br><textarea name=wgkey></textarea></label>
<p class=\"help-text\" id=\"wgkeyHelpText\">Insert your Wirguard public key here. You can use clipboard.</p>
<input type=submit value=\"Sign Up\" class=\"button\"><input type=hidden name=act value=signup></form>";
break;


case "signup":
$title="Sign up";
$rest=allnicscapacity()-activeuserscnt();
if($rest<=0){
echo "Server is full. If you wish to reserve a slot, try again later!";
break;
}

$chk=true;
$key=addslashes(trim($_REQUEST['wgkey']));

/*
$capacity=0;
foreach ($wgnics as $k=>$nic){
$adrnet=explode('/',$nic['address']);

$addr=$adrnet[0];
$netmask=$adrnet[1];

$ni=new networkinfo($addr,$netmask);

$hostscnt=$ni->intlastip-$ni->intzeroip-1;
$capacity=$capacity+$hostscnt;

//var_dump($ni);
}
*/
$capacity=allnicscapacity();
//echo $capacity;
$userscnt=activeuserscnt();


$freehosts=$capacity-$userscnt;
//echo $freehosts;
if($freehosts>0){
//exit;

if(!$key){
$errors[]="Key is empty!";
$chk=false;
}
if(strlen($key)!=44){
$errors[]="Key size is wrong!";
$chk=false;
}
if(!strlen(base64_decode($key))){
$errors[]="Key is wrong!";
$chk=false;
}

if(getkeyid($key)){
$errors[]="Key is already registred!";
$chk=false;
}


//var_dump(base64_decode($key));


if($chk){
$sql[]="insert into clients (timestamp, ip, wgkey) values (now(), \"".$ip."\", \"".addslashes($_REQUEST['wgkey'])."\")";
$sql[]="select @@identity as id";
//echo $sql;
$db->execute($sql);
$x=$db->dataset;
//var_dump($x);
$id=@$x[0]['id'];
if($id){
echo "<p>Your key is registred! Your key id is #$id.";
echo "<br>";
echo "After few minutes you can be able to get <a href=\"?act=connectionsettings&wgkey=".urlencode($_REQUEST['wgkey'])."\">connection settings</a> to VPN server.";
echo "<br>";
//echo "<a href=\"/\">Got it</a>";
}
}else{
$s="";
foreach ($errors as $err){
$s.="<li>$err</li>";
}
echo "Errors: <ul>$s</ul>";

echo "<br>";
echo "<a href=\"javascript:history.back()\">Try again</a>";
}
}else{
echo "Error: No free hosts available. Try again later.";
}

break;
case "updatereservation":
$title="Reservation update";

$rest=allnicscapacity()-activeuserscnt();
if($rest<=0){
echo "Server is full. If you wish to reserve a slot, try again later!";
break;
}

$chk=true;
$key=addslashes(trim($_REQUEST['wgkey']));

if(!$key){
$errors[]="Key is empty!";
$chk=false;
}
if(strlen($key)!=44){
$errors[]="Key size is wrong!";
$chk=false;
}
if(!strlen(base64_decode($key))){
$errors[]="Key is wrong!";
$chk=false;
}
//echo $key;

if(!getkeyid($key)){
$errors[]="Key is not registred!";
$chk=false;
}


//var_dump(base64_decode($key));


if($chk){
$sql[]="update clients set timestamp=now() where wgkey=\"".addslashes($_REQUEST['wgkey'])."\"";
$sql[]="select now() currentdate, DATE_ADD(c.timestamp, INTERVAL ".$config['reservationtime']." second) releasedate,
unix_timestamp(DATE_ADD(c.timestamp, INTERVAL ".$config['reservationtime']." second))-unix_timestamp(now()) timerest,
p.ipaddr pipaddr, i.ipaddr, i.port, i.publickey, i.gwipaddr  from clients c
left join peers p on p.id=c.id
left join interfaces i on i.id=p.interface
where wgkey=\"$key\"";
//var_dump($sql,$db->error);
$db->execute($sql);
$x=$db->dataset;
//var_dump($x);
$id=intval(@$x[0]['timerest']);
if($id>0){
echo "<p>Your access reservation is completed.";
echo "<br>";
echo "After few minutes you can be able to get <a href=\"?act=connectionsettings&wgkey=".urlencode($_REQUEST['wgkey'])."\">connection settings</a> to VPN server.";
echo "<br>";
//echo "<a href=\"/\">Got it</a>";
}
}else{
$s="";
foreach ($errors as $err){
$s.="<li>$err</li>";
}
echo "Errors: <ul>$s</ul>";

//echo "<br>";
//echo "<a href=\"javascript:history.back()\">Try again</a>";
}

break;

case "peerinfo":
$title="Connection info";
$peer=getpeer($ip);
//var_dump($peer);
$handshakeval=0;
$file=$config['wghandshakesfile'];
if(file_exists($file)){
if(time()-filemtime($file)<100){

if (($handle = fopen($file, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE) {
	if($data[0]==$peer['interface'] && $data[1]==$peer['pubkey']){
	    $handshakeval=$data[2];
	    $handshake=time()-$data[2];
	}
    }
}
}

$file=$config['wgtransferfile'];
if(file_exists($file)){
if(time()-filemtime($file)<100){

if (($handle = fopen($file, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE) {
	if($data[0]==$peer['interface'] && $data[1]==$peer['pubkey']){
	    $rx=$data[2];
	    $tx=$data[3];
	}
    }
}
}

echo "IP-address: $ip";
echo "<br>";
echo "Gateway/subnet: ".$peer['gateway'];
echo "<br>";
echo "Server date: ".$peer['currentdate']."<br>";
echo "Release date: ".$peer['releasedate']."<br>";
echo "Slot release will be occured in  ".floor($peer['timerest']/60)." minutes <br>";
if($handshakeval){
echo "<hr>";
echo "Latest handshake: $handshake sec. ago";
echo "<br>";
echo "Recieved: $rx bytes";
echo "<br>";
echo "Transfered: $tx bytes";
}

}
}else{echo "file \"".$config['wghandshakesfile']."\" not found";}


break;

case "appdiag":
$chk=array();

$chk['handshakes']=false;
$file=$config['wghandshakesfile'];
if(file_exists($file)){
if(time()-filemtime($file)<120){
$chk['handshakes']=true;
}
}

$chk['transfer']=false;
$file=$config['wgtransferfile'];
if(file_exists($file)){
if(time()-filemtime($file)<120){
$chk['transfer']=true;
}
}

foreach($config['pingtargets'] as $target){
$pingval=ping($target);
$chk['ping-'.$target]=(floatval($pingval)>0);
$pings[$target]=$pingval;
}

//var_dump($chk);
$title="Application diag";
echo "Checks: <ul>";
foreach($chk as $k=>$v){
echo "<li>".$k.": ".$v;
}
echo "</ul>";


echo "Ping values: <ul>";
foreach($pings as $k=>$v){
echo "<li>".$k.": ".$v;
}
echo "</ul>";

echo "VPN-services: <ul>";
foreach($config['vpnservices'] as $k=>$srv){
echo "<li>$k: ".(socktest($srv[0],$srv[1],$srv[2]));
}
echo "</ul>";


break;

default: 

$title="Welcome";


$peer=getpeer($ip);
//var_dump($peer);

echo "<p>Your ip address is <b>$ip</b></p>";


if(!$peer){
//echo "<p>Do you wanna <a href=\"?act=signupfrm\">sign up</a> or <a href=\"?act=connectionsettingsfrm\">get connection settings</a>?</p>";

$rest=allnicscapacity()-activeuserscnt();
if($rest>0){
echo "<p>Few steps and you connected!
<ol>
<li>Install <a href=\"https://www.wireguard.com/\" target=\"_blank\">Wireguard</a> on your device
<li>Generate private & public keys
<li><a href=\"?act=signupfrm\">Sign up</a> with your public key
<li><a href=\"?act=connectionsettingsfrm\">Get connection settings</a> for your public key
<li>Configure Wireguard
<li>Configure Firewall
<li>Connect!
</p>";
}else{
echo "Server is full. If you wish to reserve a slot, try again later!";
}


}else{
echo "<p>You are connected to VPN.</p>";
echo "<p>Now <a href=\"?act=services\">additional services</a> are become available.</p>";
}


}


$content=ob_get_contents();
ob_end_clean();


include "inc/template.php";

/*?>


<h1><?php echo $config['sitetitle'];?></h1>
<hr>

<?php echo $content;?>
*/
