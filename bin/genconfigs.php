#!/usr/bin/env php
<?php
/*
[Interface]
Address = 10.200.200.1/24
SaveConfig = true
PrivateKey = ***secret***
ListenPort = 51820

[Peer]
PublicKey = ***secret***
AllowedIPs = 10.200.200.2/32
*/

include "../inc/main.php";

$zclients=array();
$keys=array();
/*
$sql="select c.id, c.timestamp, c.wgkey `key`,
now() currentdate, DATE_ADD(c.timestamp, INTERVAL ".$config['reservationtime']." second) releasedate,
unix_timestamp(DATE_ADD(c.timestamp, INTERVAL ".$config['reservationtime']." second))-unix_timestamp(now()) timerest,
p.ipaddr pipaddr, i.ipaddr, i.port, i.publickey, i.gwipaddr, a.ipaddr allocatedaddr  from clients c
left join peers p on p.id=c.id
left join interfaces i on i.id=p.interface
left join allocation a on a.id=c.id and a.interface=i.id
";
*/
$sql="select c.id, c.timestamp, c.wgkey `key`,
now() currentdate, DATE_ADD(c.timestamp, INTERVAL ".$config['reservationtime']." second) releasedate,
unix_timestamp(DATE_ADD(c.timestamp, INTERVAL ".$config['reservationtime']." second))-unix_timestamp(now()) timerest, a.ipaddr allocatedaddr  
from clients c
left join allocation a on a.id=c.id";
$db->execute($sql);
var_dump($sql,$db->error);
$x=$db->dataset;
//var_dump($x);
foreach($x as $row){
    if(intval($row['timerest'])>0){
	$keys[]=$row;
	//array('id'=>$row['id'],'key'=>$row['wgkey'], 'allocatedaddr'=>$);
    }else{
	$zclients[]=$row['id'];
    }
}

//dealocate deprecated allocation
$sql=array();
foreach($zclients as $v){
$sql[]="delete from allocation where id=$v";
}
$db->execute($sql);
//var_dump($sql,$zclients);
//exit;



var_dump($keys);
$kk=0;
$sql=array();
$sql[]="truncate peers";
$sql[]="truncate interfaces";
$db->execute($sql);


foreach ($wgnics as $k=>$nic){
$net=explode('.',$nic['address']);
$dns="";
if(@$nic['dns']){
$dns=$nic['dns'];
}
$sql[]="insert into interfaces (name,ipaddr,port, publickey, gwipaddr,dns) values (
\"".addslashes($k)."\",\"".addslashes($nic['ipaddr'])."\",\"".addslashes($nic['port'])."\",\"".addslashes($nic['publickey'])."\",\"".addslashes($nic['address'])."\", \"".addslashes($dns)."\")";
$sql[]="select @@identity as dbid";
//var_dump($sql);
$db->execute($sql);
$x=$db->dataset;
if(is_array($x)){
$nicdbid=$x[0]['dbid'];
}
echo $nicdbid;

$configfile=$config['wgconfigdir']."/$k.conf";

$configfile2=$configfile.".update";
echo $configfile."\n";
$configfiledata="";
$configfiledata2="";

$configfiledata.="[Interface]\n";
$configfiledata.="Address = ".$nic['address']."\n";
//$configfiledata.="SaveConfig = true\n";
$configfiledata.="PrivateKey = ".$nic['privatekey']."\n";
$configfiledata.="ListenPort = ".$nic['port']."\n";
$configfiledata.="\n";

$configfiledata2="";
$configfiledata2.="[Interface]\n";
//$configfiledata.="Address = ".$nic['address']."\n";
//$configfiledata.="SaveConfig = true\n";
$configfiledata2.="PrivateKey = ".$nic['privatekey']."\n";
$configfiledata2.="ListenPort = ".$nic['port']."\n";
$configfiledata2.="\n";




$adrnet=explode('/',$nic['address']);

$addr=$adrnet[0];
$netmask=$adrnet[1];

$ni=new networkinfo($addr,$netmask);

$intfirstusableip=$ni->intzeroip+2; 
$intlastusableip=$ni->intlastip-1;
$hostscnt=$intlastusableip-$intfirstusableip+1;
//var_dump($ni, $hostscnt, printbinasip(int2binary($intfirstusableip)),printbinasip(int2binary($intlastusableip)));

//Get adresses in network
$addresses=array();
for($i=0;$i<$hostscnt;$i++){
$addresses[]=printbinasip(int2binary($intfirstusableip+$i));
}

//var_dump($addresses);

$allocatedaddresses=array();
$allocatedclients=array();
//assign allocated addressses

//In each ip address
foreach($addresses as $address){
    //Each client
    foreach($keys as $client){
	//if address should be allocated
	if($address==@$client['allocatedaddr'] ){
	    echo $client['allocatedaddr']."=>".$client['id']."\n";
	    //write config data
	    $configfiledata.="# ".$client['id']."\n";
	    $configfiledata.="[Peer]\n";
	    $configfiledata.="PublicKey = ".$client['key']."\n";
	    $configfiledata.="AllowedIPs = ".$address."/32\n";
	    $configfiledata.="\n";

	    //$configfiledata.="# ".$client['id']."\n";
	    $configfiledata2.="[Peer]\n";
	    $configfiledata2.="PublicKey = ".$client['key']."\n";
	    $configfiledata2.="AllowedIPs = ".$address."/32\n";
	    $configfiledata2.="\n";

	    //save allocated clients
	    $allocatedclients[]=$client['id'];
	    $allocatedaddresses[]=$address;

	    //save peer info in db
	    //echo $client['id']."\n";
	    $sql="insert into peers (id, ipaddr, interface) values (\"".$client['id']."\", \"$address\", \"$nicdbid\")";
	    
	    $db->execute($sql);
/*	}else{
	    //save unallocated address
	    if(!in_array($unallocatedaddresses,$address)){
	    $unallocatedaddresses[$address]=$address;
	    }
	*/    
	}
    }
}



$configfiledata.="# =========================\n";
$configfiledata.="# unallocated ipadresses   \n";
$configfiledata.="# =========================\n";
var_dump($addresses,$allocatedaddresses);

//exit;
//assign non allocated addresses
//proccess all unallocated addresses
foreach($addresses as $address){
    //if address is not allocated
    if(!in_array($address,$allocatedaddresses)){
    //each client
    foreach($keys as $client){
	//if client unallocated
	//var_dump($client);
	if(!in_array($client['id'],$allocatedclients)){
	    //write config data
	    $configfiledata.="# ".$client['id']."\n";

	    $configfiledata.="[Peer]\n";
	    $configfiledata.="PublicKey = ".$client['key']."\n";
	    $configfiledata.="AllowedIPs = ".$address."/32\n";
	    $configfiledata.="\n";

	    $configfiledata2.="[Peer]\n";
	    $configfiledata2.="PublicKey = ".$client['key']."\n";
	    $configfiledata2.="AllowedIPs = ".$address."/32\n";
	    $configfiledata2.="\n";

	    //reset sql buffer
	    $sql=array();
	    if(!$client['allocatedaddr']){
		//save allocation
		$sql[]="insert into allocation (id,ipaddr) value (\"".$client['id']."\", \"$address\")";
		$allocatedclients[]=$client['id'];
		$allocatedaddresses[]=$address;
		
	    }
	    //save peers info
	    $sql[]="insert into peers (id, ipaddr, interface) values (\"".$client['id']."\", \"$address\", \"$nicdbid\")";
	    $db->execute($sql);
	    //var_dump($sql);
	}
    }
    }
}

$kk++;

echo $configfiledata;

file_put_contents($configfile, $configfiledata);
file_put_contents($configfile2, $configfiledata2);

}






