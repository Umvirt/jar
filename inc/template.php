<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo $config['sitetitle'];?></title>
    <link rel="stylesheet" href="/css/foundation.min.css" />
  </head>
  <body>
  
  
  <div class="top-bar">
  <div class="top-bar-left">
    <ul class="dropdown menu" data-dropdown-menu>
      <li class="menu-text"><?php echo $config['sitetitle'];?></li>
      <li><a href="/">Home</a></li>

<?php
if(!getpeer($ip)){
?>
      <li>
        <a href="#">Visitor</a>
        <ul class="menu vertical">
          <li><a href="?act=signupfrm">Sign up</a></li>
          <li><a href="?act=connectionsettingsfrm">Settings</a></li>
        </ul>
      </li>

<?php
}else{
?>
      <li><a href="#">Peer</a>
        <ul class="menu vertical">
          <li><a href="?act=peerinfo">Info</a></li>
          <li><a href="?act=scanme">Scan ports on my IP</a></li>
	</ul>
	</li>
<?php
}
?>

      <li>
        <a href="#">Information</a>
        <ul class="menu vertical">
          <li><a href="?act=about">About</a></li>
          <li><a href="?act=services">Services</a></li>
          <li><a href="?act=capacity">Capacity</a></li>
          <li><a href="?act=appdiag">Status</a></li>
        </ul>
      </li>


  </div>
</div>
  
  
  
<div class="grid-container">
<?php 
if($title){
echo "<h1>$title</h1>";
}
echo $content;
?>
</div>

    <script src="/js/jquery.min.js"></script>
    <!--script src="/js/what-input.js"></script-->
    <script src="/js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>

  </body>
</html>
